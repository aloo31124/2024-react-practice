import { useCallback } from 'react';

export default function Ch2UseCallback({ productId, referrer, theme }) 
{
    const handleSubmit = useCallback((orderDetails) => {
      post('/product/' + productId + '/buy', {
        referrer,
        orderDetails,
      });}, [productId, referrer]);

    return (
        <>
            <h2>ch2. useCallback 練習</h2>
            <button onClick={handleSubmit}>按我!</button>
        </>
    )
}
