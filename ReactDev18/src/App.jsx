import './App.css'
import Ch2UseCallback from './refHook/ch2UseCallback/Ch2UseCallback'

function App() {

  return (
    <>
      <h1>官方文件練習</h1>
      <Ch2UseCallback/>
    </>
  )
}

export default App
