export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm;


-----------------------------------
react 快速創建
> npx create-react-app my-app
> npm start
-----------------------------------

vite 創建
> npm create vite
> npm install
> npm run dev
> http://127.0.0.1:5173/


> cd BroCode
> npm run dev


================================================================================
https://www.youtube.com/watch?v=CgkZ7MvWUAA

ch1.   00:00:00 React tutorial for beginners ⚛️
    1. 使用 vite 安裝, npm create vite,
    2. npm run dev 啟動 , port :


ch2.   00:20:26 card components 🃏
    1. 設計卡片樣式
    2. 引入 assets圖, 與 css
        => assets 圖 from 轉為物件
        => css 此先直接引入, 不需 from 


ch3.   00:32:24 add CSS styles 🎨
    1. css module , 使用 className, 是唯一值, class插入後會在命名,要用物件: className={style.btn1}
    2. css 為 jsx 內部物件, 適用於 該組件內變化, (?)大型應用不適合, 要用物件: style={btn2Black}
    3. import 一般 css , 要用文字: className="btn3"


ch4.   00:40:40 props📧
    1. props 是用於 父子 組件溝通
    2. [實作] 簡易 學生組件 範例 function Student(){}
    3. prop型別檢查


ch5.   00:52:49 conditional rendering ❓
    1. [需求] ch5 條件 render, 範例: 判斷是否登入顯示畫面
    2.[實作] 寫法1: 先 if-else 判斷後, 分別 return
    3. [實作] 寫法2: 先判斷好, 存入 return的 虛擬dom, 一次return => 只能放入一個 虛擬dom ="=


ch6.   01:03:04 render lists 📃 (30min)
    1. [需求][探討] render list => 呈現 水果蔬菜列表,與商邏處理 (時間略長)
    2. 程式 map 內可 處理 多個(巢狀) 虛擬dom, list虛擬dom 需要 key(unique)
    3-1. list.sort, filter 使用 => [補充] 加入 {} 需 return, 簡寫兩者都需省略
    3-2. prop 取得 app 傳入之 水果列, 蔬菜列, 標題
    4. 會有 defaultProp設定 與 PropType 檢查 => 有點懶, 略 :)


ch7.   01:29:43 click events 👆
    1. [需求] 使用 let count 計算次數, 超過三次 叫他別按了 :) 

ch8.   01:42:04 useState() hook 🎣
    1. 可改狀態 => 略 => 

ch9.   01:58:36 onChange event handler 🚦
ch10. 02:13:16 color picker app 🖌
ch11. 02:23:31 updater functions 🔄
ch12. 02:30:45 update OBJECTS in state 🚗
ch13. 02:39:55 update ARRAYS in state 🍎
ch14. 02:48:55 update ARRAY of OBJECTS in state 🚘
ch15. 03:01:42 to-do list app ☝
ch16. 03:24:17 useEffect() hook 🌟
ch17. 03:44:08 digital clock app 🕒
ch18. 04:00:08 useContext() hook 🧗‍♂️
    1. 多層 組件 A => B => C => D 資料傳遞

ch19. 04:11:44 useRef() hook 🗳️
ch20. 04:23:01 stopwatch app ⏱



