import { useState } from "react";

function PracHeader() {

    const [clickTime, _clickCounter] = useState(0);

    return (
        <div>
            複習 header
            點擊次數: {clickTime}
            <button onClick={() => _clickCounter(clickTime+1)}>點擊!</button>
        </div>
    );

}

export default PracHeader;
