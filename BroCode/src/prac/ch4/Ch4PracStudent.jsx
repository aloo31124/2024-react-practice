import "./Ch4PracStudent.css"
import PropTypes from "prop-types";

function Ch4PracStudent(props) {

    return (
        <div className="student-card">
            <h4>練習學生資訊</h4>
            <p>姓名: {props.name}</p>
            <p>年紀: {props.age}</p>
            <p>是否學生: {props.isStudent?"是":"否"}</p>
        </div>
    );

}
Ch4PracStudent.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number,
    isStudent: PropTypes.bool,
}

export default Ch4PracStudent;