import ButtonModuleStyle from "./Ch3PracButtonList.module.css";
import "./Ch3PracButtonList.css";

function Ch3PracButtonList() {

    const btn3 = {
        background: "yellow"
    }

    return (
        <div>
            <button className={ButtonModuleStyle.btn1}>btn1 module</button>
            <button className="btn2">btn2 直接引入 css</button>
            <button style={btn3}>btn3 jsx 樣式物件</button>
        </div>
    );

}

export default Ch3PracButtonList;
