import BroCodeImg from "../../assets/brocode.jpg";
import "./Ch2PracCard.css";

function Ch2PracCard() {

    return (
        <div className="prac-card">
            <img src={BroCodeImg} alt="找無" />
            <h3>練習 卡片標題</h3>
            <p>練習 卡片內文</p>
        </div>
    );

}

export default Ch2PracCard;
