
function Ch6PracList(props) {

    const title = "練習: " + props.title;
    const itemList = props.items;

    // 排序
    itemList.sort((a, b) => a.calories - b.calories);

    // 整理虛擬 dom 
    const resultList = itemList.map(item => {
        return (
            <li key={item.id}>
                <h4>{item.name}</h4>
                <a href="/">{item.calories}</a>
            </li>
        );
    })

    return (
        <>
            <h3>{title}</h3>
            <ol>{resultList}</ol>
        </>
    );

} 

export default Ch6PracList;