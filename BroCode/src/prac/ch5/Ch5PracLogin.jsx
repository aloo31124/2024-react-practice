
function Ch5PracLogin(props) {

    const homePage = (  <div>
                            <h4>歡迎登入 !!</h4>
                            <p>{props.userName}, 如下為網站功能</p>
                        </div>
                        );
    const loginPage = (
                        <>
                            <h4>請先登入</h4>
                            登入方式:....
                        </>);
                        
    const nav = <div>導覽列</div> 
    const contentPage = props.isLogin ? homePage : loginPage;

    return (  
        <>
            {nav}
            {contentPage}
        </>
    );

}

export default Ch5PracLogin;