
// 1. css module , 使用 className, 是唯一值, class插入後會在命名,要用物件: className={style.btn1}
import style from "./ButtonPink.module.css";
// 3. import 一般 css , 要用文字: className="btn3"
import "./ButtonList.css";

function ButtonList() {

    // 2. css 為 jsx 內部物件, 適用於 該組件內變化, (?)大型應用不適合, 要用物件: style={btn2Black}
    const btn2Black = {
        background: "black",
        color: "white",
    }

    return(
        <div>
            <button className={style.btn1}>按鈕1</button>
            <button style={btn2Black}>按鈕2</button>
            <button className="btn3">按鈕3</button>
        </div>
    );

}

export default ButtonList;
