
function Ch7Click() {

    let count = 0;

    const clickCounter = (e) => {
        count ++;
        console.log("count: ", count);

        console.log("e.target: ", e.target);
        console.log("e.target.textContent: ", e.target.textContent);
        console.log("e: ", e);
    }


    return (
        <>
            <h4>點選次數 {count}</h4>
            <button onClick={(e) => clickCounter(e)}>點我</button>
        </>
    );

}

export default Ch7Click;
