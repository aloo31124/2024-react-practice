import "./StudentCard.css"
import PropTypes from "prop-types";

function Student(props) {

    return(
        <div className="student-card">
            <p>
                學生姓名: {props.name}
            </p>
            <p>
                年紀: {props.age}
            </p>
            <p>
                是否為學生 {props.isStudent? "是":"否"}
            </p>
            
        </div>
    );

}

Student.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number,
    isStudent: PropTypes.bool,
}

export default Student;
