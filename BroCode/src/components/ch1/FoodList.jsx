
function FoodList() {

    const food1 = "banana";
    const food2 = "apple";

    return (
        <>
            <ul>
                <li>水果喔！</li>
                <li>{food1}</li>
                <li>{food2.toUpperCase()}</li>
            </ul>
        </>
    );

}

export default FoodList;
