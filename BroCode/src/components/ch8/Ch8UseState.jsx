import { useState } from "react";

function Ch8UseState() {

    const [name, _setName] = useState("原先姓名");

    const setName = () => {
        _setName("修改後姓名 1 ");
    }

    return (
        <>
            <h3>姓名 {name} </h3>
            <button onClick={setName}>修改名字 1 </button>
            <button onClick={() => _setName("修改後名字 2 yo")}>修改名字 2 </button>
        </>
    );

}

export default Ch8UseState;
