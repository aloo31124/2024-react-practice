import Ch18C from "../Ch18C/Ch18C";
import "../ch18.css";


function Ch18B() {

    return(
        <div className="ch18-box">
            <h3>Ch18 B</h3>
            <Ch18C/>
        </div>
    );
}

export default Ch18B;
