import { createContext, useState } from "react";
import Ch18B from "../Ch18B/Ch18B";
import "../ch18.css";

// 2. 匯出 建立之 Context容器
export const userContext = createContext();

function Ch18A() {

    // 1. 新增 state 值 userName
    const [user, _setUser] = useState("handsom loulou");

    // 3. 使用 userContext.Provider 傳遞
    return(
        <div className="ch18-box">
            <h3>Ch18 A</h3>
            <div>Hi! {user} ! yo! </div>
            <userContext.Provider value={user} >
                <Ch18B/>
            </userContext.Provider>
        </div>
    );
}

export default Ch18A;
