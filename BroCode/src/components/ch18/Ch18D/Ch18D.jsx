import { useContext } from "react";
import "../ch18.css";
import { userContext } from "../Ch18A/Ch18A";


function Ch18D() {

    const user = useContext(userContext)

    return(
        <div className="ch18-box">
            <h3>Ch18 D</h3>
            <div>Bye! {user}!</div>
        </div>
    );
}

export default Ch18D;
