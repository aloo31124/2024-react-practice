//import reactLogo from "../../assets/react.svg"
import BroCodeImg from "../../assets/brocode.jpg"
import "./Card.css" 

function Card() {

    return (
        <div className="card">
            <img src={BroCodeImg}/>
            <h3>card name</h3>
            <p>卡片內文 yoyoyo </p>
        </div>
    );

}

export default Card;