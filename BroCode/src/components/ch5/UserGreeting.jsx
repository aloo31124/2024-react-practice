
// 1. [需求] ch5 條件 render, 範例: 判斷是否登入顯示畫面
function UserGreeting(props) {

    // 2.[實作] 寫法1: 先 if-else 判斷後, 分別 return
    /* 
    if(props.isLogin) {
        return (
            <h3>歡迎登入!! {props.userName}</h3>
        );
    } else {
        return (
            <h3>請先登入喔!</h3>
        );
    }
     */

    // 3. [實作] 寫法2: 先判斷好, 存入 return的 虛擬dom, 一次return => 只能放入一個 虛擬dom ="=
    const homeDom = <h3>歡迎登入!! {props.userName}</h3>;
    const loginDom = <h3>請先登入喔!</h3>;
    const resultDom = props.isLogin ? homeDom : loginDom;

    return (resultDom);
}
// propTypes 檢查 => 偷懶, 略 :)
export default UserGreeting;
