
// 1. [需求][探討] render list => 呈現列表的 一些商邏處理 (時間略長)
function List(props) {

    // -------------   前半部寫法 start -------------
    // 該資料後續回移至 app
    const fruits = [{id:1 ,name: "香蕉", calories: 211}, 
                    {id:2 ,name: "蘋果", calories: 22}, 
                    {id:3 ,name: "梨子", calories: 137}, 
                    {id:4 ,name: "草莓", calories: 4}];

    // 3-1. list.sort, filter 使用 => [補充] 加入 {} 需 return, 簡寫兩者都需省略
    //fruits.sort((a, b) => a.name.localeCompare(b.name));
    //fruits.sort((a,b) => { return a.calories - b.calories}); // 需 return
    fruits.sort((a,b) => a.calories - b.calories); // 簡寫不需
    const lowFruits = fruits.filter(fruit => {return fruit.calories < 200});
    // -------------   前半部寫法 end -------------


    // 3-2. prop 取得 app 傳入之 水果列, 蔬菜列, 標題
    const title = props.title
    const itemList = props.items;


    // 2. 程式 map 內可 處理 多個(巢狀) 虛擬dom, list虛擬dom 需要 key(unique)
    const resultList = itemList.map(item => {
        return (
            <li> 
                <h4 key={item.id}>{item.name}</h4>
                <a href="">{item.calories}</a>
            </li>
        );
    });

    return(
        <div>
            <h2>{title}</h2>
            <ol>{resultList}</ol>
        </div>
    );

}
// 4. 會有 defaultProp設定 與 PropType 檢查 => 有點懶, 略 :)
export default List;
