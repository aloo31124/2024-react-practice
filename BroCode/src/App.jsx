import { useState } from 'react'
import './App.css'

// 實作
import Header from './components/ch1/Header'
import Footer from './components/ch1/Footer'
import FoodList from './components/ch1/FoodList'
import Card from './components/ch2/Card'
import ButtonList from './components/ch3/ButtonList'
import Student from './components/ch4/Student'
import UserGreeting from './components/ch5/UserGreeting'
import List from './components/ch6/List'
import Ch7Click from './components/ch7/Ch7Click'
import Ch8UseState from './components/ch8/Ch8UseState'

// 練習 
// 7/2,.. ch1 ~ ch6
import PracHeader from './prac/ch1/PracHeader'
import PracFoodList from './prac/ch1/PracFoodList'
import PracFooter from './prac/ch1/PracFooter'
import Ch2PracCard from './prac/ch2/Ch2PracCard'
import Ch3PracButtonList from './prac/ch3/Ch3PracButtonList'
import Ch4PracStudent from './prac/ch4/Ch4PracStudent'
import Ch5PracLogin from './prac/ch5/Ch5PracLogin'
import Ch6PracList from './prac/ch6/Ch6PracList'
import Ch18A from './components/ch18/Ch18A/Ch18A'


function App() {
  //const [count, setCount] = useState(0)
  //ch4
  const studentObj = {name:"曉東", age:29, isStudent:true};
  //ch5 登入
  const [isLogin, _toggleLogin] = useState(false);
  //ch6 水果, 蔬菜列表
  const fruits = [{id:1 ,name: "香蕉", calories: 211}, 
                  {id:2 ,name: "蘋果", calories: 122}, 
                  {id:3 ,name: "梨子", calories: 137}, 
                  {id:4 ,name: "草莓", calories: 104}];
  
  const vegetables = [{id:1 ,name: "地瓜耶", calories: 81}, 
                      {id:2 ,name: "小白菜", calories: 22}, 
                      {id:3 ,name: "茄子", calories: 66}, 
                      {id:4 ,name: "高麗菜", calories: 91}];


  return (
    <>
      <h2>ch1 基本樣式練習</h2>
      <PracHeader/>
      <PracFoodList/>
      <PracFooter/>

      <h2>ch2 卡片</h2>
      <Ch2PracCard/>
      <Ch2PracCard/>
      <Ch2PracCard/>
      <Ch2PracCard/>

      <h2>ch3 style module button</h2>
      <Ch3PracButtonList/>

      <h2>ch4 props 父子組件溝通</h2>
      <Ch4PracStudent name="阿翔" age={33} isStudent={true}/>
      <Ch4PracStudent name="阿柔" age={32} isStudent={true}/>
      <Ch4PracStudent key="gg123" {...studentObj} />

      <h2>ch5 條件render, 範例: 判斷是否登入顯示畫面</h2>
      <button onClick={() => _toggleLogin(!isLogin)}>登入</button>
      <Ch5PracLogin isLogin={isLogin} userName="盧森堡" />
      

      <h2>ch6 render list 水果列表</h2>
      {(fruits.length > 0)        ? <Ch6PracList items={fruits} title="水果列"/> : ""}
      {(vegetables.length > 0)    ? <Ch6PracList items={vegetables} title="蔬菜列"/> : ""}

      <h2>ch7 點選事件</h2>
      <Ch7Click/>

      <h2>ch8 使用 useState</h2>
      <Ch8UseState/>


      <h2>ch18. useContext 多層父子傳遞</h2>
      <Ch18A/>

    </>
  )
}

export default App
