import './App.css';
import TodoAddForm from './components/TodoAddForm';
import TodoList from './components/TodoList';
import TypeListBar from './components/TypeListBar';
import {useState} from 'react'

function App() {
  let todoTypeList = [
    {id:"-1", typeName:"全部", isDelete: false},
    {id:"01", typeName:"工作", isDelete: false},
    {id:"02", typeName:"個人", isDelete: false},
  ]
  const [selectType, setSelectType] = useState(todoTypeList[0]);
  let _todoItemList = [
    {id:"01", type:"01", title:"盤點tdym草稿功能", isDelete:false},
    {id:"02", type:"01", title:"釐清tdym api", isDelete:false},
    {id:"03", type:"02", title:"冥想五分鐘放鬆 :) ", isDelete:false},
    {id:"04", type:"02", title:"伏地挺身一組~", isDelete:false},
  ];
  const [todoItemList, setTodoItemList] = useState(_todoItemList);
  const selectTypeFun = (id) => {
    setSelectType(todoTypeList.filter(type => type.id === id)[0]);
  }
  const addTodoItem = (newTodoObj) => {
    setTodoItemList([...todoItemList, newTodoObj])
  }
  return (
    <div className="App">
      <h1>first todo</h1>
      <TypeListBar todoTypeList={todoTypeList} selectTypeFun={selectTypeFun}/>
      <TodoList todoItemList={todoItemList} selectType={selectType}/>
      <TodoAddForm addTodoItem={addTodoItem} selectType={selectType}/>
    </div>
  );
}

export default App;
