import { useRef } from "react";
import {nanoid} from "nanoid";
import TypeTab from "./TypeTab";

function TodoAddForm(props) {
    const inputRef = useRef({});
    const addTodoItem = () => {
        const title = inputRef.current.value;
        inputRef.current.value = "";
        props.addTodoItem({id:nanoid(), type:props.selectType.id, title: title, isDelete:false});
    }
    return(
        <div>
            <input type="text" placeholder="輸入事項" ref={inputRef}/>
            <button onClick={addTodoItem}>送出</button>
            <TypeTab {...props.selectType} />
        </div>
    );
}
export default TodoAddForm;