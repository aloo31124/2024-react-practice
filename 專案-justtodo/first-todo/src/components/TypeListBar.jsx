import TypeTab from "./TypeTab";
import TypeListBarCss from "./TypeListBar.module.css"

function TypeListBar(props) {
    const selectType = (id) => {
        props.selectTypeFun(id);
    }
    return (
        <div className={TypeListBarCss.TypeList}>
            {
                props.todoTypeList.map(type => {
                    return <TypeTab key={type.id} {...type} selectType={selectType}/>
                })
            }
        </div>
    );
}
export default TypeListBar;