import TodoItem from "./TodoItem";

function TodoList(props) {
    return (
        <div>
            {
                props.todoItemList
                    .filter(todo => todo.type === (props.selectType.id) || props.selectType.id === "-1")
                    .map(todo => {
                        return <TodoItem key={todo.id} {...todo}/>
                    })
            }
        </div>
    );
}
export default TodoList;