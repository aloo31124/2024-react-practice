import TypeTabCss from "./TypeTab.module.css";

function TypeTab(props) {
    const selectType = () => {
        if(!props.selectType) {
            return;
        }
        props.selectType(props.id);
    }
    return (
        <div className={TypeTabCss.TypeTab} onClick={selectType}>
            {props.typeName}
        </div>
    );
}
export default TypeTab;
