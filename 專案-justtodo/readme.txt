

================================================================================
目錄:
> cd 專案-justtodo
-----------------------------------
> cd first-todo         react 基本todo練習
> cd first-app-expo     react repo todo 嘗試


================================================================================
## 專案11月目標:
=> just todo web  => 新增刪除 item 存在 地端
=> jsut todo expo app => 新增刪除 item 存在 地端

-----------------------------------
細項:
=> first-todo 呈現樣式, type 篩選   => 10/30 完成 
=> first-todo 呈現樣式, 新增, 刪除  => 11/3



================================================================================
專案-justtodo 建立專案:
-----------------------------------
> npx create-react-app first-todo
> cd first-todo
> npm start

-----------------------------------
> npx create-expo-app@latest
    ? What is your app named? › my-app > first-app-expo
> cd first-app-expo
> nvm use 20.0.0
> npx expo start --web
> npx expo start --android
> npx expo start --ios



================================================================================






================================================================================
兩種建制 react 方式 (舊的語法筆記)
-----------------------------------
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm;

-----------------------------------
react 快速創建 (採用)
> npx create-react-app first-todo
> cd first-todo
> npm start
http://localhost:3000/

-----------------------------------
vite 創建
> npm create vite
> npm install
> npm run dev
> http://127.0.0.1:5173/


