

> export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm;

> cd 02-小g老師2/03-redux-login
> npx create-react-app 03-redux-login
> npm start

============================================================================================
redux 登入範例參考資料:
https://www.youtube.com/watch?v=IgFtEGXjl7Y


1. 基本建置與 元件
> components
>> HomePage.jsx
>> LoginPage.jsx
>> Profile.jsx


2. npm 下載相關 庫
> npm install @reduxjs/toolkit
> npm install react-redux


3. App 新增 store(redux) , Provider (可在 step 6 完成)


4. 建立 使用者登入 [狀態片段] userSlice
=> 使用 create Slice
=> 新增檔案 /store/userSlice.jsx 
=> 建立 const userSlice = createSlice({})
=> initialState, /* 建立初始狀態 */
=> reducers:  /* 狀態改變之動作函式 */
==> 函式: setLogin, setLogout; 參數: state, action


5. export userSlice.js
=> export 使用 解構之方式, 將 *slice action , 解構出 reducer內之 處理狀態函式
=> export reducer ? ="=

    export const {setLogin, setLogout} = userSlice.actions;
    export default userSlice.reducer;


6. App 新增 store(redux) , Provider, userSlice
=> reducer 使用 userSlice
    const store = configureStore({
        reducer: {
            /* key: value */
            user: userSlice,
        }
    });
    ...
    <Provider store={store}>



7. 使用 useSelector 取 定義的state 狀態 [是否登入] 於 Profile.jsx 撰寫
=> const state = useSelector(state => state.user);
=> { state.profile.login ? <HomePage/> : <LoginPage/> }



8. 使用 useDispatch 取 定義的函式, [登入] 後狀態改變與相關動作, 於 LoginPage.jsx 撰寫
=> 相關欄位使用 ref 容器存儲, 並取值
=> 按下 [登入] 使用 dispatch 調用 setLogin()函式
    const dispatch = useDispatch();
    fun() { ...
        dispatch(setLogin({
            name: nameValue,
            age: ageValue,
            mail: emailValue,
        }));
    }



9. reudx slice 狀態切片, 取得 action

            /* 解構 action.payload */
            const {name, age, mail} = action.payload;
            state.profile = {
                name,
                age,
                mail,
                login: true,
            };



10. 使用 之前所學 useSelector, useDispatch 於 HomePage.jsx 實作 1.呈現使用者資訊 2.登出




============================================================================================

reducer 內不能使用 async/await, 需將非同步動作寫在外層
    https://weiyun0912.github.io/Wei-Docusaurus/docs/React/Package/Redux-Toolkit/#action-creator


