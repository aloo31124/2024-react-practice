import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    profile: {
        name: "",
        age: 0,
        mail: "",
        login: false,
    }
}

const userSlice = createSlice({
    name: "user",
    initialState: initialState, /* 建立初始狀態 */
    reducers: { /* 狀態改變之動作函式 */
        setLogin(state, action) {
            // console.log(action); /* action 為需要依靠的外部參數傳進去 */

            /* 解構 action.payload */
            const {name, age, mail} = action.payload;
            state.profile = {
                name,
                age,
                mail,
                login: true,
            };
        },
        setLogout(state) {
            /* 登出, 不需使用 外部函式, 因此不使用 action */
            state.profile = initialState;
        }
    }
});

/* export 使用 解構之方式, 將 *slice action , 解構出 reducer內之 處理狀態函式 */
export const {setLogin, setLogout} = userSlice.actions;
export default userSlice.reducer;
