import React, {useRef} from "react";
import {useDispatch} from "react-redux";
import { setLogin } from "../store/userSlice";

function LoginPage() {
    //8. 使用 useDispatch 取 定義的函式, [登入] 後狀態改變與相關動作, 於 LoginPage.jsx 撰寫
    const dispatch = useDispatch();

    /* 相關欄位使用 ref 容器存儲, 並取值 */
    const mailRef = useRef();
    const nameRef = useRef();
    const ageRef = useRef();

    /* 按下 [登入] 使用 dispatch 調用 setLogin()函式 */
    const handleLogin = () => {
        const emailValue = mailRef.current.value;
        const nameValue = nameRef.current.value;
        const ageValue = ageRef.current.value;
        dispatch(setLogin({
            name: nameValue,
            age: ageValue,
            mail: emailValue,
        }));
    }

    return (
        <div>
            <h3>登入畫面</h3>
            信箱: <input type="text" ref={mailRef}/>
            <br />
            姓名: <input type="text" ref={nameRef}/> 
            <br />
            年齡: <input type="text" ref={ageRef}/> 
            <br />
            <button onClick={handleLogin}>登入</button>
        </div>
    )
}
export default LoginPage;