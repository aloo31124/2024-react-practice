
import { useSelector, useDispatch } from "react-redux";
import { setLogout } from "../store/userSlice";

function HomePage() {
    const {name, age, mail} = useSelector(state => state.user.profile);
    const dispatch = useDispatch();
    const handleLogout = () => {
        dispatch(setLogout());
    }

    return (
        <div>
            <h3>歡迎登入</h3>
            登入資訊
            <br />
            姓名: {name}
            <br />
            信箱: {mail}
            <br />
            年齡: {age}
            <br />
            <button onClick={handleLogout}>登出</button> 
        </div>
    )
}
export default HomePage;