
import LoginPage from "./LoginPage";
import HomePage from "./HomePage";
import { useSelector } from "react-redux";


function Profile() {
    const state = useSelector(state => state.user);
    console.log(" profile state : ", state);
    return (
        <div>
            <h2>redux 範例</h2>
            { state.profile.login ? <HomePage/> : <LoginPage/> }
        </div>
    )
}
export default Profile;