import { configureStore } from "@reduxjs/toolkit";
import { Provider } from 'react-redux'
import Profile from "./components/Profile";
import userSlice from "./store/userSlice";

/*
  store 內之 
    redux 是 設定如何操作 state 之 fun
 */
const store = configureStore({
  reducer: {
    /* key: value */
    user: userSlice,
  }
});

function App() {
  return (
    <Provider store={store}>
      <Profile/>
    </Provider>
  );
}

export default App;
