import './App.css';
import React, {Component} from "react";
import axios from "axios";
import List from './components/list';
import Search from './components/search';

class App extends React.Component {

  /*
   * ch65 代理伺服器 
   */
  getStudentData = () => {
    console.log("get!yo!");
    axios.get("http://localhost:3000/students").then(
      response => {
        console.log("取得資訊: ", response);
      },
      error => {
        console.log("取得失敗 QQ : ", error);
      }
    )
  }

  render() {
    return (
      <div>
        <h2>ch65 代理伺服器測試</h2>
        <button onClick={this.getStudentData}>點擊獲取學生數據</button>

        <h2>ch67 github 請求</h2>
        <Search/>
        <List/>
      </div>
    );
  }
}

export default App;
