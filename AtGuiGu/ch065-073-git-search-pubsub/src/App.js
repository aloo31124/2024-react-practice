import './App.css';
import Search from './components/Search';
import List from './components/List';

function App() {
  return (
    <div>
      <h1>github 搜尋範例</h1>
      <Search/>
      <List/>
    </div>
  );
}

export default App;
