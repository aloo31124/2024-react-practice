import React, {Component} from 'react';
import axios from 'axios';

export default class Search extends Component {
    /* 
     * 送出搜尋請求
     */
    search = () => {
        /* 
            // 寫法1, 取 value
            console.log("input: ", this.keywordInput.value);
            const {value} = this.keywordInput;
            console.log(" keyword vale : ", value);
         */

        // 寫法2: 使用連續結構賦值取 ref
        // 切勿連續點擊
        const {keywordInput: {value: keyword} } = this;
        console.log("連續結構賦值取 : ", keyword);
        axios.get("https://api.github.com/search/users?q=" + keyword)
            .then(res => { console.log("成功: ", res)})
            .catch(error => { console.log("error", error) });      
    }

    render() {
        return (
            <div>
                <h2>github搜尋bar</h2>
                <input ref={c => this.keywordInput = c } type="text" placeholder='請輸入搜尋關鍵字'/>
                <button onClick={this.search}>搜尋</button>
            </div>
        );
    }
}