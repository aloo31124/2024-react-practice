/* 
> export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm;

> cd AtGuiGu/ch065-node-server
> node server1

localhost:8888/students

*/

const express = require("express");
const app = express();

app.use((request, response, next) => {
    console.log("有人請求服務器1");
    next();
});


app.get("/students", (request, response) => {
    const students = [
        {id:"001", name:"tome", age:18},
        {id:"002", name:"jaja", age:22},
        {id:"003", name:"tt", age:25},
    ]
    response.send(students);
});

app.listen(8888, (err) => {
    if(!err) {
        console.log("服務器1啟動成功拉!");
    } else {
        console.log("err: ", err);
    }
})

