
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm;


> cd AtGuiGu/ch116-126-extension
> npm start



ch 118 尚硅谷 react教程 扩展3 stateHook
    https://www.youtube.com/watch?v=dKvAhZX_Q9E&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=118

1. 複習 [函式組件], [類組件]
    1. [函式組件] 使用 state, ref, props 與 hook 的關係? ans: hook可讓[函式組件] 使用 state, ref ; 而 props 本身就可使用參數傳入函式使用
    2. [函式組件] 是因為沒有何者, 造成無法取用 state, ref? ans: this => 因為嚴格模式, [函式組件] 無 this
    3. [函式組件] 缺少的 console.log 印出為何? ans: [函式組件] this 印出為 undefined

    
2. [函式組件] 狀態加總 state 簡易實作
    4. [實作][函式組件] function DemoStateHook() => 點選按鈕, 可計算點擊次數
    5. [實作][step1] 新增 React.useState(0), 存於 數組[...,...] , 該數組兩參數為何? 數組其實就是?  ans: [狀態值, 修改函數], 數組就是陣列array
    6. [實作][step2] 新增 onClick={add} 函數, 修改 state
    7. [探討] useState(0) 每次 點選都會被執行, 值是否會被重複初始為0? 為何? ans: 不會, 因為 React底層已幫忙處理

3. [函式組件][實作延伸1] set state 2+1 種寫法
    8. [探討]useState(0) 會賦值給 數組[], 其中修改state函數 _add() 的寫法可傳入哪兩種? ans: 傳入參數, 傳入函式
    9. [實作]修改state函數 _add() 2+1 種寫法, 請實作:
        _add(countTime+1);                              // 傳入參數
        _add((countTime) => {return countTime+1});      // 傳入函式
        _add(countTime => countTime+1);                 // 傳入函式

4. [函式組件][實作延伸2] 修改多個 state
    10. [探討]若該 [函式組件] 有多個狀態, useState 該如何調整? ans: 只能重複寫 useState 賦值數組
    11. [實作] 點選按鈕後, Tom 改 Jack => 練習前建議幾範例整個砍掉



119 尚硅谷 react教程 扩展4 EffectHook
https://www.youtube.com/watch?v=_9D-t6EE7vs&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=119

1. 概述
    1. effectHook 中, effect 本身指 效果, 副作用 => 恩, 沒啥用的知識點 :) 
    2. effectHook 此鉤子是讓 [函數組件] 可使用 [類組件]的何者? ans: 新的生命週期
    3. [函數組件] 內是否可直接使用 [生命週期鉤子]? ans: 不可 廢話 :)

2. [實作] 跳秒器 => 每秒數字+1 => 跳秒暴衝
    4. [實作][step1] 建立一個 DemoEffectHook() 之 [類組件] , return 虛擬元件 + export default
    5. [實作][step2] useState(initVal) 存入數組 [countTime, setCount], 並 return 顯示秒數
    6. [實作][step3] useEffect(()=> {}) 內使用 setInterval(() => {})
    7. [探討] 上述 step3 會發生甚麼事? 為何? ans: 秒數會越跳越快, 因為每次 state更新 useEffect 都會執行
    8. [實作][step4] 上述探討 秒數爆衝的問題, useEffect(()=> {}) 該如何解決? ans: 加入 [監測數組] : useEffect(()=> {}, [])

3. [探討] useEffect(()=> {}, []) => [監聽數組]
    9. [探討] useEffect(()=> {}, []) [監聽數組] 是哪一個? ans: [監聽數組]是 useEffect 後面的 []
    10. [探討]  有無監聽數組差在哪? ans: useEffect(()=> {}) 無, 全部監聽 ;  useEffect(()=> {}, []) 有空陣列, 則全不監聽



120 尚硅谷 react教程 扩展5 RefHook
https://www.youtube.com/watch?v=vkPHsh0iqMc&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=120

1. [複習][實作][類組件] => 使用ref取input值, 點選按鈕後 alert 顯示 input 值 
    1. [實作] 視情況複習該範例: 創建容器 => show()該容器input值 => render() + ref + btn onClick
    2. [複習] ref 分哪三種? 發展順序? ans: 字串ref => 回調ref => ref創建容器
    3. [探討] refHook 偏向上述哪一種? ans: ref創建容器

2. [複習][實作][函式組件] => 使用ref取input值, 點選按鈕後 alert 顯示 input 值
=> 步驟與 [類組件] 相似
    4. [實作][step1] 使用 React.useRef() 創建容器 inputContainer  (後續簡稱useRef)
    5. [實作][step2] show()函式 alert 顯示 容器中 input內值
    6. [實作][step3] render() + ref + btn onClick

