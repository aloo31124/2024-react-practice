import React, { Component, lazy, Suspense } from "react";
import {NavLink, Routes, Route, BrowserRouter} from "react-router-dom";
//import Home from "./home";
//import About from "./about";

/* 
ch117
    https://www.youtube.com/watch?v=tu0D9SZ84mI&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=117

1. 前言, 路由範例:  
    1. 懶加載為何? 何種情境使用? ans: 需要時再載入, 如: 路由情境時
    2. 範例預備: 路由下載 => [法1], yarn add react-router-dom => 需下載 yarn ="= ; [法2] npm install react-router-dom
    => 使用 npm
    3. [實作][探討] 路由顯示基本分三個區塊: BrowserRouter, NavLink, Routes-Route
    => ans: NavLink:上方導覽列,  Routes-Route:顯示路由切換內容,  BrowserRouter: 包覆最外層
    4. [實作] Home, About 組件 進行路由切換
2. Home, About 組件 使用 懶加載

=> 錯誤 :) TypeError: Cannot read properties of undefined (reading 'then')

*/

const Home = lazy(() => { import("./home") });
const About = lazy(() => { import("./about") });

export default class DemoLazyLoad extends Component {

    render() {
        return (
            <div>
                <BrowserRouter>
                    <h2>117 尚硅谷 react教程 扩展2 lazyLoad</h2>
                    <h3>註冊路由, nav: </h3>
                    <NavLink to="/about">About</NavLink>
                    <br />
                    <NavLink to="/home">Home</NavLink>
                    <h3>切換內容:</h3>
                    <Suspense fallback={<h3>Loading...</h3>}>
                        <Routes>
                            <Route path="/about" Component={About}/>
                            <Route path="/home" Component={Home}/>
                        </Routes>
                    </Suspense>
                </BrowserRouter>
            </div>
        );
    }
}