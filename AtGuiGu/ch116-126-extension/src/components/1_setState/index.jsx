import React, { Component } from "react";


/* 
    ch116
    https://www.youtube.com/watch?v=pwZgxyBvbdM&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=116

    1. [實作] 請設計出 計數器

    // 一, [實作] add1 對象式寫法
    2. setState 執行完後對 state更新動作 是 同步/非同步? ans: 非同步
    3. 如下 add 函數, setState 與 回調函數內, 印出 count 差別? ans: 回調是 state 修改後的

    // 二, [實作] add2 函數式寫法
    4. setState 函數式寫法 第一個參數為何? ans: 函數
    5. 上述該 函數 可取哪兩者? state, props
    6. 提示: this.setState((state, props) => {...});

    // 三, [探討] 使用建議:
    7. 新狀態 不依賴於 原狀態, 建議使用? 反之? ans:  不依賴使用[對象式]; 反之使用[函數式]
        => 提示1: [對象式]: this.setState({count:99});
        => 提示2: [函數式]: this.setState(() => ({count:99}));
    8. 新狀態 依賴於 原狀態, 適合使用? ans: 函數式(是建議)
        => 提示1: [函數式] this.setState(state => ({count: state.count+1}));
    9. setState [對象是],[函數式] 兩種寫法, 哪一個有 [回調函數]? 幹嘛用的? ans: 兩個都有; 可看修改後的值

*/

export default class DemoSetState extends Component {

    state = {count:0}

    // 一, add1 對象式寫法
    add1 = () => {
        //  2. setState 執行完後對 state更新動作 是 同步/非同步? 
        //  3. 如下 add 函數, setState 與 回調函數內, 印出 count 差別?s
        let {count} = this.state;
        this.setState({count: count+1},() => {
            console.log("setState回調內", this.state.count);
        });
        console.log("setState後", this.state.count);
    }

    // 二, add2 函數式寫法
    add2 = () => {
        this.setState((state, props) => {
            return {count:state.count+1}
        })

        // this.setState(state => ({count:state.count+1}));
    }

    render() {
        return (
            <div>
                <h2>116 尚硅谷 react教程 扩展1 setState </h2>
                求和範例, 當前總和: {this.state.count}
                <br />
                <button onClick={this.add2}>點擊+1</button>
            </div>
        );
    }
}