import React, { Component } from "react";

/* 
ch 118 尚硅谷 react教程 扩展3 stateHook
    https://www.youtube.com/watch?v=dKvAhZX_Q9E&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=118

1. 複習 [函式組件], [類組件]
    1. [函式組件] 使用 state, ref, props 與 hook 的關係? ans: hook可讓[函式組件] 使用 state, ref ; 而 props 本身就可使用參數傳入函式使用
    2. [函式組件] 是因為沒有何者, 造成無法取用 state, ref? ans: this => 因為嚴格模式, [函式組件] 無 this
    3. [函式組件] 缺少的 console.log 印出為何? ans: [函式組件] this 印出為 undefined

    
2. [函式組件] 狀態加總 state 簡易實作
    4. [實作][函式組件] function DemoStateHook() => 點選按鈕, 可計算點擊次數
    5. [實作][step1] 新增 React.useState(0), 存於 數組[...,...] , 該數組兩參數為何? 數組其實就是?  ans: [狀態值, 修改函數], 數組就是陣列array
    6. [實作][step2] 新增 onClick={add} 函數, 修改 state
    7. [探討] useState(0) 每次 點選都會被執行, 值是否會被重複初始為0? 為何? ans: 不會, 因為 React底層已幫忙處理

3. [函式組件][實作延伸1] set state 2+1 種寫法
    8. [探討]useState(0) 會賦值給 數組[], 其中修改state函數 _add() 的寫法可傳入哪兩種? ans: 傳入參數, 傳入函式
    9. [實作]修改state函數 _add() 2+1 種寫法, 請實作:
        _add(countTime+1);                              // 傳入參數
        _add((countTime) => {return countTime+1});      // 傳入函式
        _add(countTime => countTime+1);                 // 傳入函式

4. [函式組件][實作延伸2] 修改多個 state
    10. [探討]若該 [函式組件] 有多個狀態, useState 該如何調整? ans: 只能重複寫 useState 賦值數組
    11. [實作] 點選按鈕後, Tom 改 Jack => 練習前建議幾範例整個砍掉
        
*/

/* 
export default class DemoSetState extends Component {
    render() {
        return (
            <div>
                <h2>118 尚硅谷 react教程 扩展3 stateHook</h2>
            </div>
        );
    }
}
 */


function DemoStateHook() {
    // 3. [函式組件] 缺少的 console.log 印出為何?
    console.log("DemoStateHook 執行, 並且 this: ", this);
    
    // 5. [實作][step1] 新增 React.useState(0), 存於 數組[...,...] , 該數組兩參數為何? 數組其實就是?
    // 7. [探討] React.useState(0) 每次 點選都會被執行, 值是否會被重複初始為0? 為何?
    const [countTime, _add] = React.useState(0);
    const [showName, _changeName] = React.useState("tom");

    // 6. [實作][step2] 新增 onClick={add} 函數, 修改 state
    function add() {
        // 9. [實作]修改state函數 _add() 2+1 種寫法, 請實作:
        //_add(countTime+1);
        //_add((countTime) => {return countTime+1});
        _add(countTime => countTime+1);
    }

    // 11. [實作] 點選按鈕後, Tom 改 Jack => 練習前建議幾範例整個砍掉
    function changeName() {
        _changeName("jack");
    }

    return (
        <div>
            <h2>118 尚硅谷 react教程 扩展3 stateHook</h2>
            <h3>點擊數: {countTime} </h3>
            <button onClick={add}>點擊+1</button>
            <h3>Tom 改 Jack, 名稱: {showName} </h3>
            <button onClick={changeName}>修改</button>
        </div>
    );
}
export default DemoStateHook;
