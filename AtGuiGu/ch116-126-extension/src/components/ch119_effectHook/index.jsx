import React, { Component } from "react";

/* 
119 尚硅谷 react教程 扩展4 EffectHook
https://www.youtube.com/watch?v=_9D-t6EE7vs&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=119

1. 概述
    1. effectHook 中, effect 本身指 效果, 副作用 => 恩, 沒啥用的知識點 :) 
    2. effectHook 此鉤子是讓 [函數組件] 可使用 [類組件]的何者? ans: 新的生命週期
    3. [函數組件] 內是否可直接使用 [生命週期鉤子]? ans: 不可 廢話 :)

2. [實作] 跳秒器 => 每秒數字+1 => 跳秒暴衝
    4. [實作][step1] 建立一個 DemoEffectHook() 之 [類組件] , return 虛擬元件 + export default
    5. [實作][step2] useState(initVal) 存入數組 [countTime, setCount], 並 return 顯示秒數
    6. [實作][step3] useEffect(()=> {}) 內使用 setInterval(() => {})
    7. [探討] 上述 step3 會發生甚麼事? 為何? ans: 秒數會越跳越快, 因為每次 state更新 useEffect 都會執行
    8. [實作][step4] 上述探討 秒數爆衝的問題, useEffect(()=> {}) 該如何解決? ans: 加入 [監測數組] : useEffect(()=> {}, [])

3. [探討] useEffect(()=> {}, []) => [監聽數組]
    9. [探討] useEffect(()=> {}, []) [監聽數組] 是哪一個? ans: [監聽數組]是 useEffect 後面的 []
    10. [探討]  有無監聽數組差在哪? ans: useEffect(()=> {}) 無, 全部監聽 ;  useEffect(()=> {}, []) 有空陣列, 則全不監聽

 */ 

/* 
export default class DemoEffectHook extends Component {
    render() {
        return (
            <div>
                <h2>119 尚硅谷 react教程 扩展4 EffectHook</h2>
            </div>
        );
    }
}

 */


// 4. [實作][step1] 建立一個 DemoEffectHook() 之 [類組件] , return 虛擬元件 + export default
function DemoEffectHook() {
    // 5. [實作][step2] useState(initVal) 存入數組 [countTime, setCount], 並 return 顯示秒數
    const [countTime, _setCount] = React.useState(0);

    // 6. [實作][step3] useEffect(()=> {}) 內使用 setInterval(() => {})
    React.useEffect(() => {
        // 7. [探討] 上述 step3 會發生甚麼事? 為何? ans: 秒數會越跳越快, 因為每次 state更新 useEffect 都會執行
        
        setInterval(() => {
            _setCount(countTime => countTime+1);
        }, 1000);
         
    },[]);

    return (
        <div>
            <h2>119 尚硅谷 react教程 扩展4 EffectHook</h2>
            <h3>秒數跳: {countTime}</h3>
        </div>
    );
}
export default DemoEffectHook;