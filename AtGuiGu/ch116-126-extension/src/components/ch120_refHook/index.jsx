import React, { Component } from "react";

/* 
120 尚硅谷 react教程 扩展5 RefHook
https://www.youtube.com/watch?v=vkPHsh0iqMc&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=120

1. [複習][實作][類組件] => 使用ref取input值, 點選按鈕後 alert 顯示 input 值 
    1. [實作] 視情況複習該範例: 創建容器 => show()該容器input值 => render() + ref + btn onClick
    2. [複習] ref 分哪三種? 發展順序? ans: 字串ref => 回調ref => ref創建容器
    3. [探討] refHook 偏向上述哪一種? ans: ref創建容器

2. [複習][實作][函式組件] => 使用ref取input值, 點選按鈕後 alert 顯示 input 值
=> 步驟與 [類組件] 相似
    4. [實作][step1] 使用 React.useRef() 創建容器 inputContainer  (後續簡稱useRef)
    5. [實作][step2] show()函式 alert 顯示 容器中 input內值
    6. [實作][step3] render() + ref + btn onClick
*/

/* 
//1. [複習][實作][類組件] => 使用ref取input值, 點選按鈕後 alert 顯示 input 值 
export default class DemoRefHook extends Component {
    // 1. [實作] 視情況複習該範例: 創建容器 => show()該容器input值 => render() + ref + btn onClick
    inputContainer = React.createRef();
    show = () => {
        alert(this.inputContainer.current.value);
    }
    render() {
        return (
            <div>
                <h2>120 尚硅谷 react教程 扩展5 RefHook</h2>
                <h3>[複習][實作][類組件] 使用 ref 讓 alert 顯示 input 值</h3>
                <input ref={this.inputContainer} type="text" placeholder="alert顯示"/>
                <button onClick={this.show}>alert 顯示輸入值</button>
            </div>
        );
    }
}
 */

function DemoRefHook() {
    const inputContainer = React.useRef();

    function show() {
        alert(inputContainer.current.value);
    }

    return (
        <div>
            <h2>120 尚硅谷 react教程 扩展5 RefHook</h2>
            <h3>[複習][實作][函式組件] 使用ref取input值, 點選按鈕後 alert 顯示 input 值</h3>
            <input ref={inputContainer} type="text" placeholder="輸入內容" />
            <button onClick={show}>alert顯示輸入內容</button>
        </div>
    );
}
export default DemoRefHook;


