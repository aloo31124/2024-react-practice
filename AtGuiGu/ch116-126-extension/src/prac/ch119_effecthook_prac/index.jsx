import React from "react";

function PracEffectHook() {

    const [count, addCount] = React.useState(0);

    /*
     * 生命週期 讀秒數 
     */
    React.useEffect(() => {
        setInterval(() => {
            addCount(count => count+1);
        }, 1000);
    },[]); // 補入空的監測器, 不重複計數

    return (
        <div>
            <h2>ch119 effect hook 生命週期,秒數暴衝</h2>
            當前秒數: {count}
        </div>
    );

}

export default PracEffectHook;
