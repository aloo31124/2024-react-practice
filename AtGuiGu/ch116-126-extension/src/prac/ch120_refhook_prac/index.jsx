import React from "react";

function PracRefHook() {

    const inputContainer = React.useRef();
    
    function showInput() {
        alert(inputContainer.current.value);
    }

    return(
        <div>
            <h2>ch120 ref hook 事件, 容器</h2>
            <input type="text" placeholder="輸入內容" ref={inputContainer}/>
            <button onClick={showInput}>顯示輸入</button>
        </div>
    );

}

export default PracRefHook;