import React from "react"

function PracStateHook() {

    const [countTime1, add] = React.useState(0);
    const [countTime2, _add2] = React.useState(0);
    const [countTime3, _add3] = React.useState(0);
    const [userName, _changeName] = React.useState("jack");

    /* 
     * 寫法1: useState 輸入參數
     */
    function add1() {
        add(countTime1+1);
    }

    /*
     * 寫法2: useState 輸入函式
     */
    function add2() {
        _add2((countTime2) => {return countTime2 + 1});
    }

    /*
     * 寫法3: useState 輸入函式 簡寫
     */
    function add3() {
        _add3(countTime3 => countTime3 + 1);
    }

    /* 
     * jack 改為 tom
     */
    function changeName() {
        _changeName("tom");
    }

    return(
        <div>
            <h2>[複習] ch118 setState Hook </h2>
            <h3>寫法1: useState 輸入參數</h3>
            <p>法1 點擊次數: {countTime1}</p>
            <button onClick={add1}>點擊計算次數</button>

            <h3>寫法2: useState 輸入函式</h3>
            <p>法2 點擊次數 {countTime2}</p>
            <button onClick={add2}>點擊計算</button>

            <h3>寫法3: useState 輸入函式 簡寫</h3>
            <p>法3 點擊次數 {countTime3}</p>
            <button onClick={add3}>點擊計算</button>

            <h3>jack 改為 tom</h3>
            <p>你好, 我叫 {userName} </p>
            <button onClick={changeName}>改名</button>
        </div>
    );

}

export default PracStateHook;