import React, { Component } from "react";
// 原先寫法
import DemoSetState from "./components/1_setState";
import DemoLazyLoad from "./components/2_lazyLoad";
import DemoStateHook from "./components/ch118_stateHook";
import DemoEffectHook from "./components/ch119_effectHook";
import DemoRefHook from "./components/ch120_refHook";

// 練習使用
import PracStateHook from "./prac/ch118_statehook_prac";
import PracEffectHook from "./prac/ch119_effecthook_prac";
import PracRefHook from "./prac/ch120_refhook_prac";


export default class App extends Component {
    render() {
        return (
            <div>
                <PracStateHook/>
                <PracEffectHook/>
                <PracRefHook/>
            </div>
        );
    }
}