import './App.css';
import React, {Component} from "react"; 
import Header from './components/header';
import List from './components/list';
import Footer from './components/footer';

class App extends React.Component{
  state = {
    todoList: [
      {id:"cccc000", name:"放鬆冥想 :) ", isDone:true},
      {id:"001", name:"啞鈴重訓", isDone:false},
      {id:"002", name:"space in ios 上架", isDone:true},
      {id:"003", name:"艾官方網站js重構", isDone:true},
    ]
  };

  newTodo = (newTodoObj) => {
    //console.log("app組件", newTodoObj);
    const {todoList} = this.state;
    const newTodoList = [...todoList, newTodoObj];
    this.setState({todoList: newTodoList});
  }

  updateTodo = (id, isDone) => {
    //console.log(id, isDone);
    const {todoList} = this.state;
    const newTodoList = todoList.map(todoObj => {
      if(id === todoObj.id) {
        return {...todoObj, isDone};
      }
      else {
        return todoObj;
      }
    });
    this.setState({todoList: newTodoList});
  }

  deleteTodo = (id) => {
    //console.log("app delete id: ", id);
    const {todoList} = this.state;
    const newTodoList = todoList.filter(todoObj => {
      return todoObj.id !== id
    });

    this.setState({todoList: newTodoList});
  }

  changeAllDone = (isDone) => {
    //console.log("app changeAllDone ");
    const {todoList} = this.state;
    const newTodoList = todoList.map(todoObj => {
      return {...todoObj, isDone:isDone}
    });
    this.setState({todoList: newTodoList});
  }

  deleteIsDone = () => {
    console.log("app deleteIsDone");
    const {todoList} = this.state;
    const newTodoList = todoList.filter((todoObj) => !todoObj.isDone);
    this.setState({todoList: newTodoList});
  }

  render() {
    const {todoList} = this.state;
    return (
      <div>
        <h1>Todo</h1>
        <Header newTodo={this.newTodo}/>
        <List todos={todoList} updateTodo={this.updateTodo} deleteTodo={this.deleteTodo}/>
        <Footer todos={todoList} changeAllDone={this.changeAllDone} deleteIsDone={this.deleteIsDone}/>
      </div>
    );
  }
}

export default App;
