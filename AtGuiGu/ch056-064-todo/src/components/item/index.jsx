import React, {Component} from "react";
//import "./index.css"

class Item extends React.Component {

    state = {isMouseInItem: false};

    mouseEnter = (isEnter) => {
        //console.log(isEnter);
        return () => {
            //console.log(isEnter);
            this.setState({isMouseInItem: isEnter});
        }
    }

    changeDone(id) {
        return (event) => {
            this.props.changeDone(id, event.target.checked);
        }
    }

    deleteTodo = (id) => {
        //console.log("item id: ", id);
        if(window.confirm("是否刪除?")){
            this.props.deleteTodo(id);
        }
    }

    render() {
        const {id, name, isDone} = this.props;
        const {isMouseInItem} = this.state;
        return (
            <div style={{background: isMouseInItem ? "#ddd" : "white" }}
                 onMouseEnter={this.mouseEnter(true)} onMouseLeave={this.mouseEnter(false)}>
                <input type="checkbox" checked={isDone} onChange={this.changeDone(id)}/>
                事項:{name}
                <button onClick={()=> this.deleteTodo(id)} style={{display:isMouseInItem ? "inline":"none"}}>刪除</button>
            </div>
        );
    }
}

export default Item;