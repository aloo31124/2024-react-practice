import React, {Component} from "react";
import Item from "../item";
import PropType from "prop-types";

class List extends React.Component {

    static propType ={
        todos: PropType.array.isRequired,
        updateTodo: PropType.func.isRequired,
    }

    render() {
        const {todos, updateTodo, deleteTodo} = this.props;
        return (
            <div>
                <h2>待做清單:</h2>
                {
                    todos.map(todo => {
                        return <Item key={todo.id} {...todo} changeDone={updateTodo} deleteTodo={deleteTodo}/>
                    })
                }
            </div>
        );
    }
}

export default List;