import React, {Component} from "react";
import {nanoid} from "nanoid";
import PropTypes from "prop-types";


class Header extends React.Component {

    static propTypes = {
        //addTodo: PropTypes.func.isRequired,
        newTodo: PropTypes.func.isRequired,
    }

    addTodo = (event) => {
        const {target, keyCode} = event;
        // 若按下 enter 才處理
        const keyEnterCode = 13;
        if(keyCode !== keyEnterCode) {
            return;
        }
        //console.log("header組件", target.value);
        this.props.newTodo({id:nanoid(), name:target.value, isDone:false});
    }

    render() {
        return (
            <div>
                <h2>header, 輸入任務: </h2>
                <input onKeyUp={this.addTodo} type='text' placeholder='輸入任務'></input>
            </div>
        );
    }
}

export default Header;