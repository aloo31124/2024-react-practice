import React, {Component} from "react";

class Footer extends React.Component {

    changeAllDone = (event) => {
        this.props.changeAllDone(event.target.checked);
    }

    deleteIsDone = () => {
        this.props.deleteIsDone();
    }

    render() {
        const {todos} = this.props;
        const total = todos.length;
        const doneCount = todos.reduce((pre, todo) => {return pre + (todo.isDone? 1:0)}, 0);

        return (
            <div>
                <h2>footer, 小結:</h2>
                全選: <input type="checkbox" onChange={this.changeAllDone} checked={total === doneCount && total !== 0}/>
                已完成 {doneCount} / 全部 {total} 
                <button onClick={this.deleteIsDone}>刪除已完成</button>
            </div>
        );
    }
}

export default Footer;