

> export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm;

建立
> npx create-react-app AtGuiGu/ch073-089-router

啟動
> cd AtGuiGu/ch073-089-router
> npm start

> npm i react-router-dom



=============================================================================
077 尚硅谷 react教程 路由的基本使用
https://www.youtube.com/watch?v=fsvAkVbRLkA&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=77

=> 新寫法：

      <BrowserRouter>

        <Link to="/home">
          <h2>首頁</h2>
        </Link>
        <Link to="/about">
          <h2>關於</h2>
        </Link>
        
        <Routes>
          {/* 使用 element prop 來指定要渲染的組件 非component */}
          <Route path="/home" element={<Home />} />
          <Route path="/about" element={<About />} />
        </Routes>

      </BrowserRouter>







