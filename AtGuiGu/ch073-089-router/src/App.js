import './App.css';
import {BrowserRouter, Link, Route, Routes} from 'react-router-dom';
import Home from './components/Home';
import About from './components/About';

function App() {
  return (
    <div>
      <h1>路由練習</h1>
      <BrowserRouter>
        <Link to="/home">
          <h2>首頁</h2>
        </Link>
        <Link to="/about">
          <h2>關於</h2>
        </Link>
        
        <Routes>
          {/* 使用 element prop 來指定要渲染的組件 非component */}
          <Route path="/home" element={<Home />} />
          <Route path="/about" element={<About />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
