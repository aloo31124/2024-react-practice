
ch056-064-todo

> export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")" 
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm;  

> cd AtGuiGu/ch056-064-todo-prac
> npm start


056 尚硅谷 react教程 TodoList案例 静态组件
https://www.youtube.com/watch?v=GkAU0n4hWp4&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=56

1. 需求, 規劃
    1. todo list , 每個項目有刪除按鈕, 勾選視為完成
    2. 組件該如何拆分建議? (無正確答案) ans: header, list, item, footer
    3. 組件拆分原則? (無正確答案) ans: 應該盡量以功能

2. [實作] 基本建制
    4. src 建置所有組件
    5. app.js 設定 header, list, item
    6. list 內設定 item => 寫死資料 



057 尚硅谷 react教程 TodoList案例 动态初始化列表
https://www.youtube.com/watch?v=jj5seGyN_oI&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=57

1. [實作] step1. todo案例, 當前知識只適合 [父子]組件傳遞, todo數據要存於哪個組件? ans: app組件 (header,list 的父組件)
=> 溝通方式: [兄弟]組件 溝通, 後續章節會提 (好像是 消息訂閱)
=> 假資料: 使用 todos[] 陣列: [{id:"001", name:"啞鈴重訓", isDone:false}, ... ]

2. [實作] step2. [父子]組件溝通 app => list => item 使用 [類組件] 的何者? ans: 使用 props
=> 提示1: item 需要 key => diff 概念
=> 提示2: props 善用展開符 {...x}

3. [實作] step3. list組件 中展開todos陣列, 並於 item組件顯示 
=> 提示1: todos.map(todo => { return <>...</> });

4. [實作] step4. item組件 顯示[todo]內容: input-checkbox, name
=> 提示1: input-checkbox 中, checked 與 defaultChecked 差異? ans: checked 賦值就不可再修改, 
=> 提示2: defaultChecked 後續會有 bug, 但後續會說 (那講屁)



058 尚硅谷 react教程 TodoList案例 添加todo
https://www.youtube.com/watch?v=dRGUHegy9HU&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=58

1. [實作] step1. input 偵測 js key 事件 輸入文字後, 按下 enter 才開始處理: 
=> 提示1: 事件 onKeyDown, 與 onKeyUp 何者比較合適? ans: onKeyUp => 鍵盤收起
=> 提示2: 按下 enter keyCode 為多少? ans: 13
=> 提示3: 如何從 input 簡潔的取 target, keyCode? ans: const {target, keyCode} = event;


2. [實作] step2. header組件新增後, 傳值(todo名稱)給 app組件
=> 提示1: [子]header 如何傳值給 [父]app (子傳父)? ans: header 使用 props + app 使用 賦值 ()=>{}

3. [實作] step3. 承上一步, 將 header 傳給 app 的 todo名稱 改為 todoObj , 並存入 app 陣列
=> 提示1: header組件 產生一個 todoObj, 請問 id 建議可用何種庫避免重複? ans: nanoid (UUID) 
> npm i uuid
> npm i nanoid

=> 提示2: app組件 從 state 取 todoList 需拆開陣列, 重新組合 => const newTodoList = [...todoList, newTodoObj];




059 尚硅谷 react教程 TodoList案例 鼠标移入效果
https://www.youtube.com/watch?v=qbovhN6GOpA&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=59

1. [需求]: todo item 滑鼠移入, 高亮 + 刪除按鈕顯示

2. [略] [寫法1]: css + hover => 參考 item/index.css 檔即可
.todo-item:hover        {...高亮顏色}
.todo-item:hover .btn   {...按鈕顯示}
=> 已會寫, 視情況自行練習


3. [實作][寫法2]: js 控制, 使用 onMouseEnter/Leave 觸發, 注意 ref 回調函式! 如下實作


4. [實作][step1] item 偵測滑鼠移入, mouseEnter函式 處理:
=> 提示1: onMouseEnter={this.mouseEnter(true)} onMouseLeave={this.mouseEnter(false)}
=> 提示2: [探討] mouseEnter函式 會印出為何？ ==>  ans: 首次渲染印出一堆值, 且 滑鼠移入沒反應

5. [探討] step1 => 指定事件回調(callback), 若使用 () 執行函式, 需回傳何?  ans: 函式
=> 提示:
    <input onChange={this.changeFun(id)}>
    changeFun(id) = () => {
        return () => {} 
    }

6. [實作][step2] 偵測滑鼠移入/出, 指定事件回調, mouseEnter函式 retrun 一個 箭頭函式, 並 this.setState 修改狀態
=> 提示1: state = {isMouseInItem: false}; => 以此判斷變化


7. [實作][step3] 依照 state 修改樣式
=> 提示1: style={{background: isMouseInItem ? "#ddd" : "white" }}





060 尚硅谷 react教程 TodoList案例 添加一个todo
https://www.youtube.com/watch?v=77-F8fWn1Io&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=60

1. [需求] todo 項目勾選後, 需確實修改 isDone=true, 並且改變 footer 完成數量

2. [實作][step1] checkbox(ipnut) 被勾選時，changeDone(id) 可取得 該 todo 之 id, 與是否勾選
=> 提示1: [探討] checkbox 被勾選 建議使用 何種 js 觸發事件? ans: onChange
=> 提示2: id 需 state 取, 並直接給 changeDone(id)

3. [探討][複習] 指定事件回調(callback), 若使用 () 執行函式, 需回傳何?  ans: 函式
=> 提示:
    <input onChange={this.changeFun(id)}>
    changeFun(id) = () => {
        return (event) => {} 
    }

4. [探討] checkbox(input) 被勾選後, event 該如何取? 可印出看看 ans: event.target.checked 不是 value


5. [實作][step2][複習] 子傳父傳孫 組件溝通, item組件 => list組件 => app組件
=> 提示1: 注意三層的溝通, 中間的 list 組件不需要 this : <Item ... changeDone={updateTodo} />


6. [實作][step3] app組件 在接收到 item 修改項目後, 更新 todo
=> 提示1: 舊的 todoList 使用 map 比對, 並判斷key
=> 提示2: obj 更新部分屬性寫法:
obj1 = {a:11, b:22};
obj1 = {...obj1, a:13} // => a屬性被更新為 13 :)





061 尚硅谷 react教程 TodoList案例 对props进行限制
https://www.youtube.com/watch?v=uEz7t-BpBVE&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=61

1. [需求] header, list 傳入值需檢查 型別

2. [實作][stpe1] => [複習] react 檢查 型別, 需安裝 庫
=> 安裝 prop type 檢查型別庫   > npm i prop-types
=> import 該庫: import PropTypes from "prop-types";


3. [實作][stpe2] => header 加入檢查:
=> 提示1:   static propTypes = { newTodo: PropTypes.func.isRequired,}

4. [實作][stpe3] => list 加入檢查:
=> 提示:
    static propType ={
        todos: PropType.array.isRequired,
        updateTodo: PropType.func.isRequired,
    }







062 尚硅谷 react教程 TodoList案例 删除一个todo
https://www.youtube.com/watch?v=_MMXRdEXwbc&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=62

1. [實作][step1] 刪除todo, 回調函式 改新寫法, 將 回調函式 寫在 html 標籤內
=> 提示1: <button onClick={ ()=> this.deleteTodo(id) } ...>刪除</button>

2. [實作][step2] [複習] app => list => item prop 溝通
=> 提示1: prop type 複習型別檢查


3. [實作][step3] 於 app.js 之 todoList 依照 id 刪除該 todo
=> 提示1: todoList[] 數組 何種方法可快速依照 id 剔除不需要之值?  ans: filter


4. [實作][step4] 使用 confirm 詢問時否刪除?
=> 探討1: app, list, item 建議在哪個組件詢問? ans: item組件
=> 探討2: confirm 無法直接使用, 需要加上何者前綴字?  ans: window.confirm






063 尚硅谷 react教程 TodoList案例 实现底部功能
https://www.youtube.com/watch?v=kZ5S9wlfsx4&list=PLmOn9nNkQxJFJXLvkNsGsoCUxJLqyLGxu&index=63

1. [需求1] app 將 todoList 傳入 footer 並計算並顯示 總數, 與 isDone完成數

2. [實作] step1. footer 計算 總數, isDone, 並顯示
=> 提示1: 計算 isDone 總數, 可使用 reduce, [複習] => todos.reduce((pre, todo) => {return pre + (todo.isDone? 1:0)}, 0);

3. [實作] step2 [需求2] 當 全部 todo 勾選時, footer 之 checkbox 會自動 勾選,
=> 提示1: app 將 todoList 傳入給 list, 即可自動判斷
=> 提示2: footer 之 checkbox 該使用 checked 還是 defaultChecked 判斷? ans: checked


4. [實作] step3 [需求3] 當 footer 之 checkbox 勾選時, 全部todo 都會被勾選
=> 提示1: app 先寫個 基本的 全部改為 isDone:true 函數: changeAllDone
=> 提示2: 後續會發現, 全選勾選後, todoList 有改, 但為何 item 沒變, 要如何改? ans: defaultChecked 改為 checked
=> 提示3: 全部取消勾選, footer 該如何修改? ans: footer 需回傳是否勾選事件: event.target.checked


5. [實作] step4 按鈕 [刪除已完成] => 主要為 複習, 因為之前都寫過 :)
=> 提示1: [複習] app組件, todoList 可使用 filter 篩選



note1. [探討] footer 之 checkbox 沒有使用 onChange 會出現如下錯誤:
    Warning: You provided a `checked` prop to a form field without an `onChange` handler. 
    This will render a read-only field. If the field should be mutable use `defaultChecked`. 
    Otherwise, set either `onChange` or `readOnly`.

note2. 若全都刪除, 全部按鈕如何取消勾選?


