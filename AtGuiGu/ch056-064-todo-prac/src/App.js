import logo from './logo.svg';
import './App.css';
import React, {Component} from "react";
import Header from './components/header';
import Footer from './components/footer';
import List from './components/list';

class App extends React.Component {

  state = {
    todoList: [
      {id:"001", name:"早晨冥想放鬆", isDone:true},
      {id:"002", name:"下班洗衣服", isDone:false},
      {id:"003", name:"晚上通話跟家人聊天", isDone:true},
      {id:"004", name:"看一本有趣放鬆讀物,兩頁", isDone:true},
    ]
  }

  /*
   * 新增項目 
   */
  newTodo = (todo) => {
    const {todoList} = this.state;
    this.setState({todoList: [...todoList, todo]})
  }

  /*
   * 刪除項目 
   */
  deletTodo = (id) => {
    console.log(" app delete id : ", id);
    const {todoList} = this.state;
    const newTodoList = todoList.filter(todo => todo.id !== id);
    this.setState({todoList: newTodoList});
  }
  

  render() {
    const {todoList} = this.state;
    return (
      <div>
        <Header newTodo={this.newTodo}/>
        <List todoList={todoList} deletTodo={this.deletTodo}/>
        <Footer/>
      </div>
    );
  }
}

export default App;