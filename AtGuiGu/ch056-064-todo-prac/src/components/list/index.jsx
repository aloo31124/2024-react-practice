import React, {Component} from "react";
import Item from "../item";
import PropTypes from "prop-types";

class List extends React.Component {

    render() {
        const {todoList, deletTodo} = this.props;
        return (
            <div>
                list 清單:
                {
                    todoList.map(todo => 
                        <Item key={todo.id} todo={todo} deletTodo={deletTodo}/>
                    )
                }
            </div>
        );
    }
}


export default List;