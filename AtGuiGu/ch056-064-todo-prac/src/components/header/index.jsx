import React, {Component} from "react";
import {nanoid} from "nanoid";
import PropTypes from "prop-types";

class Header extends React.Component {

    /*
     * 新增 todo 
     */
    addTodo = (event) => {
        const {target, keyCode} = event;
        if(keyCode !== 13) {
            return;
        }
        this.props.newTodo({id:nanoid(), name:target.value, isDone:false});
    }

    render() {
        return (
            <div>
                輸入代辦: 
                <input onKeyUp={this.addTodo} type="text" placeholder="輸入代辦事項"/>
            </div>
        );
    }
}


export default Header;