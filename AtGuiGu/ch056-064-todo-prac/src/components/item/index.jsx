import React, {Component} from "react";

class Item extends React.Component {

    /*
     * 刪除項目 
     */
    deletTodo = (id) => {
        this.props.deletTodo(id);
    }

    render() {
        const {todo} = this.props;
        return (
            <div>
                <input type="checkbox" defaultChecked={todo.isDone} />
                標題: {todo.name}
                <button onClick={() => this.deletTodo(todo.id)}>刪除</button>
            </div>
        );
    }
}


export default Item;