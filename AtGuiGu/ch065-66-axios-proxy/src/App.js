import './App.css';
import axios from 'axios';

function App() {
  
  /* 
   * ch65, 代理伺服器
   */
  const getStudentData = () => {
    console.log("get data start!! ");
    axios.get("http://localhost:8888/students")
      .then(res => {
        console.log("成功", res);
      })
      .catch(error => {
        console.log("error!! ", error);
      });
  }

  return (
    <div>
      <h1>ch65,66 非同步</h1>
      <button onClick={getStudentData}>取資料</button>
    </div>
  );
}

export default App;
